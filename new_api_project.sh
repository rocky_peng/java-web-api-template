#!/usr/bin/env bash
usage="need 3 params: groupId artifactId projectPath"


if [ ! -n "$1" ] ;then
    echo $usage
    exit 1;
else
    a=1
fi

if [ ! -n "$2" ] ;then
    echo $usage
    exit 1;
else
    a=1
fi

if [ ! -n "$3" ] ;then
    echo $usage
    exit 1;
else
    a=1
fi

echo "groupId:$1"
echo "artifactId:$2"
echo "projectPath:$3"

projectDir="$3/$1"
cp -r . $projectDir
cd $projectDir
rm -rf ./.idea/
rm -rf ./leyongleshi-web-api-template.iml
rm -rf ./.git/
rm -rf ./new_api_project.sh

cmd1="find . -name 'pom.xml' | xargs perl -pi -e 's|lygroupId|$1|g'"
eval $cmd1

cmd2="find . -name 'pom.xml' | xargs perl -pi -e 's|lyartifactId|$2|g'"
eval $cmd2

cmd3="find app-config -name 'local.properties' | xargs perl -pi -e 's|lyLogDir|$projectDir/logs|g'"
eval $cmd3

