package com.leyongleshi.soft.xxx.controller.api;

import com.leyongleshi.commons.web.BaseController;
import com.leyongleshi.commons.web.GeneralResponse;
import com.leyongleshi.commons.web.auth.AnonApi;
import com.leyongleshi.commons.web.auth.LoginRequiredApi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pengqingsong
 * @date 2018/7/24
 * @desc
 */
@RestController
@RequestMapping("/test")
public class TestController extends BaseController {

    @RequestMapping("/anon")
    @AnonApi
    public GeneralResponse test() {
        return GeneralResponse.successResponse("anon");
    }

    @RequestMapping("/need_login")
    @LoginRequiredApi
    public GeneralResponse test2() {
        return GeneralResponse.successResponse("need_login");
    }

    @RequestMapping("/open")
    public GeneralResponse test3() {
        return GeneralResponse.successResponse("open");
    }
}
